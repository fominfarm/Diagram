﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Clinic
{
    public class Doctor : Human
    {
        public Department Department { get; private set; }

        public Patient Patient { get; private set; }

        public int Salary { get; private set; }

        public Registration Registration { get; private set; }

        public Doctor(long idno, string firstname, string lastname, DateTime birthDate, Gender gender, string address,
            string contactPhone)
            : base(idno, firstname, lastname, birthDate, gender, address, contactPhone)
        {
        }

        public Doctor(long idno, string firstname, string lastname, DateTime birthDate, Gender gender, string address)
            : base(idno, firstname, lastname, birthDate, gender, address)
        {
        }

        public override string ToString()
        {
            return string.Format("Doctor \n IDNO: {0} \n FirstName: {1} \n LastName: {2}\n BirthDate: {3} \n Gender: {4} \n Address: {5}",
                IDNO,
                FirstName,
                LastName,
                BirthDate.ToShortDateString(),
                Gender,
                Address);


        }
    }
}