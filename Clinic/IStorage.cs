﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Clinic
{
    interface IStorage<T>
    {
        List<T> FromXML(string filename);
        void ToXML(List<T> obj, string filename);
    }
}
