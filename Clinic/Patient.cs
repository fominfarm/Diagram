﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Clinic
{
    public class Patient : Human
    {
        public List<Diseases> Diseases { get; private set; }

        public Registration Registration { get; private set; }

        public Patient(long idno, string firstname, string lastname, DateTime birthDate, Gender gender, string address,
            string contactPhone)
            : base(idno, firstname, lastname, birthDate, gender, address, contactPhone)
        {
            Diseases = new List<Diseases> ();
        }

        public Patient(long idno, string firstname, string lastname, DateTime birthDate, Gender gender, string address)
            : base(idno, firstname, lastname, birthDate, gender, address)
        {
            Diseases = new List<Diseases>();
        }

        public override string ToString()
        {
            return
                string.Format(
                    "Patient \n IDNO: {0} \n FirstName: {1} \n LastName: {2}\n BirthDate: {3} \n Gender: {4} \n Address: {5} \n ContactPhone {6} \n Patien has next disease: \n{7}",
                    IDNO,
                    FirstName,
                    LastName,
                    BirthDate.ToShortDateString(),
                    Gender,
                    Address,
                    ContactPhone,
                    Diseases.Count == 0 ? "Diseases not registered" : String.Join("\n",Diseases.Select(d=>d.ToString()).ToArray()));
        }

        public void AddDisease(Diseases dis)
        {
            try
            {
                Diseases.Add(dis);
            }
            catch (NullReferenceException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}