﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Clinic
{
    public class Diseases
    {
        List<string> Medicine { get; set; }

        public string DiseaseName { get; private set; }

        public Diseases(string diseaseName, List<string> medicine)
        {
            DiseaseName = diseaseName;
            Medicine = medicine;
        }

        public override string ToString()
        {
            return string.Format("-- Disease: {0}\n --- Medications: {1}\n",
                DiseaseName,
                string.Join(", ",Medicine.ToArray())).TrimEnd(',');
        }
    }
}