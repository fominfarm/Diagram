﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Clinic
{
    class Program
    {
        static void Main(string[] args)
        {
            Doctor[] doctors =
            {
                new Doctor(1234567891234, "Ivanov", "Vasile", new DateTime(1950, 2, 3), Gender.Masculin,
                    "mun.Chisinau, bd.Dacia 11", "022554478"),
                new Doctor(1234567891235, "Cazacu", "Ana", new DateTime(1951, 3, 4), Gender.Feminin,
                    "mun.Chisinau, bd.Dacia 12", "022554467"),
                new Doctor(1234567891236, "Bunescu", "Ian", new DateTime(1952, 4, 5), Gender.Masculin,
                    "mun.Chisinau, bd.Dacia 13", "022554456"),
                new Doctor(1234567891237, "Zara", "Olga", new DateTime(1953, 5, 6), Gender.Feminin,
                    "mun.Chisinau, bd.Dacia 14", "022554445"),
                new Doctor(1234567891238, "Dub", "Pentru", new DateTime(1954, 6, 7), Gender.Masculin,
                    "mun.Chisinau, bd.Dacia 15", "022554434"),
            };

            Patient[] patients =
            {
                new Patient(1234567891234, "Pacient1", "Vasile", new DateTime(1940, 2, 3), Gender.Masculin,
                    "mun.Chisinau, bd.Stefan cel Mare 21", "022554477"),
                new Patient(1234567891234, "Pacient2", "Ana", new DateTime(1941, 3, 4), Gender.Feminin,
                    "mun.Chisinau, bd.Stefan cel Mare 22", "022554466"),
                new Patient(1234567891234, "Pacient3", "Ian", new DateTime(1942, 4, 5), Gender.Masculin,
                    "mun.Chisinau, bd.Stefan cel Mare 23", "022554455"),
                new Patient(1234567891234, "Pacient4", "Olga", new DateTime(1943, 5, 6), Gender.Feminin,
                    "mun.Chisinau, bd.Stefan cel Mare 24", "022554444"),
                new Patient(1234567891234, "Pacient5", "Pentru", new DateTime(1944, 6, 7), Gender.Masculin,
                    "mun.Chisinau, bd.Stefan cel Mare 25", "022554433"),
            };

            Diseases[] diseases =
            {
                new Diseases("Diagnoza1", new List<string> {"Med1 10mg comp. N20", "Med2 2% sol.inj. N10"}),
                new Diseases("Diagnoza2", new List<string> {"Med3 1% ung. 5g", "Med4 50mg comp. N5"}),
                new Diseases("Diagnoza3", new List<string> {"Med5 10g caps. N10"})
            };

            try
            {
                patients[0].AddDisease(diseases[0]);
                patients[0].AddDisease(diseases[1]);

                patients[1].AddDisease(diseases[2]);

                patients[2].AddDisease(diseases[3]);
            }
            catch (IndexOutOfRangeException ex)
            {
                Console.WriteLine(ex.Message);
            }
            

            foreach (Doctor doc in doctors)
            {
                Console.WriteLine(doc);
            }

            Console.WriteLine("--------------------------Patients-------------------------");

            foreach (Patient pat in patients)
            {
                Console.WriteLine(pat);
            }


            Array.Sort(doctors, Human.SortByFirtName());

            Console.WriteLine("---------sorted by icomparer- firstname-------");
            foreach (var doctor in doctors)
            {
                Console.WriteLine(doctor);
            }

            Console.WriteLine("---------sorted by lambda- lastname-------");
            foreach (var doctor in doctors.OrderBy(d=>d.LastName))
            {
                Console.WriteLine(doctor);
            }

            File.WriteAllLines(@"D:\testclinic.txt",doctors.Select(d=>d.ToString()).ToArray());

            //istorage

            Console.ReadLine();
            
        }
    }
}