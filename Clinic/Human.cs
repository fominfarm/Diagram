﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Clinic
{
    public abstract class Human
    {
        public long IDNO { get; private set; }

        public string FirstName { get; private set; }

        public string LastName { get; private set; }

        public DateTime BirthDate { get; private set; }

        public Gender Gender { get; private set; }

        public string Address { get; private set; }

        public string ContactPhone { get; private set; }

        protected Human(long idno, string firstName, string lastName, DateTime birthDate, Gender gender, string address)
        {
            if (idno.ToString().Length != 13)
                throw new System.ArgumentException("IDNO gresit!", "idno");
            IDNO = idno;
            FirstName = firstName;
            LastName = lastName;
            BirthDate = birthDate;
            Gender = gender;
            Address = address;
        }

        protected Human(long idno, string firstName, string lastName, DateTime birthDate, Gender gender, string address,
            string contactPhone) : this(idno, firstName, lastName, birthDate, gender, address)
        {
            ContactPhone = contactPhone;
        }

        public virtual void setContactPhone(string contactPhone)
        {
            ContactPhone = contactPhone;
        }

        public static IComparer SortByFirtName()
        {
            return (IComparer) new HumanFirstNameComparer();
        }

        public static IComparer SortByLastName()
        {
            return (IComparer) new HumanLastNameComparer();
        }

        public static IComparer SortByAge()
        {
            return (IComparer) new HumanAgeComparer();
        }
    }

    internal class HumanFirstNameComparer : IComparer
    {
        public int Compare(object x, object y)
        {
            return String.CompareOrdinal(((Human) x).FirstName, ((Human) y).FirstName);
        }
    }

    internal class HumanLastNameComparer : IComparer
    {
        public int Compare(object x, object y)
        {
            return String.CompareOrdinal(((Human) x).LastName, ((Human) y).LastName);
        }
    }

    internal class HumanAgeComparer : IComparer
    {
        public int Compare(object x, object y)
        {
            if (((Human) x).BirthDate < ((Human) y).BirthDate)
                return 1;
            if (((Human) x).BirthDate > ((Human) y).BirthDate)
                return -1;
            return 0;
        }
    }
}